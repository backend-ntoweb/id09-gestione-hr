package app.nike.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.nike.models.Hr;
import app.nike.service.HrService;

@RestController
@RequestMapping("/hr")
public class HrController {
	
	@Autowired HrService hrService;
	
	@PostMapping("/aggiungi")
	public Hr aggiungiHr(@Valid @RequestBody Hr hr) {
		return hrService.addHr(hr);
	}
	
	@PostMapping("/modifica")
	public Hr modificaHr(@Valid @RequestBody Hr hr) {
		return hrService.modificaHr(hr);
	}
	
	@DeleteMapping("/cancella/{username}")
	public boolean deleteByUsername(@PathVariable(value = "username") String username){
		 return hrService.cancellaHr(username);
	}
	
	@GetMapping("/lista")
		public List<Hr> leggiHr(){
			return hrService.trovaTutti();
		}
		
	

}
