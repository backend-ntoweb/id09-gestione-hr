package app.nike.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import app.nike.models.Email;

@FeignClient(name="id07-gestione-email", path="/email")
public interface GestioneEmailFeignClient {
	
	@PostMapping("/invia")
	public void inviaEmailProvaDiverso(@RequestBody Email emailDaInviare);

}
