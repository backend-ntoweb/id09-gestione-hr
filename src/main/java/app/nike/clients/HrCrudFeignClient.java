package app.nike.clients;



import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import app.nike.models.Hr;

@FeignClient(name = "id10-crud-hr", path="/hr")
public interface HrCrudFeignClient {

	
	@GetMapping("/{username}")
	public Hr getByUsername(@PathVariable(value = "username") String username);

	@PostMapping("/creaHr")
	public Hr salvaHr(Hr hr);

	@DeleteMapping("/cancella/{username}")
	public void rimuoviHr(@PathVariable("username")String username);

	@GetMapping("/leggi")
	public List<Hr> leggiHr();
	
	@PutMapping("/update")
	public Hr updateHr(@RequestBody Hr hr);
	
	
	
}
