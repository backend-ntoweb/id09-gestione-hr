package app.nike.service;

import java.util.List;

import app.nike.models.Hr;

public interface HrService {

	public Hr addHr(Hr hr);

	public Hr modificaHr(Hr hr);

	public List<Hr> trovaTutti();

	public boolean cancellaHr(String username);

}
