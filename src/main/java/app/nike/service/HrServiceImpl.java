package app.nike.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.nike.clients.GestioneEmailFeignClient;
import app.nike.clients.GestioneUtenteFeignClient;
import app.nike.clients.HrCrudFeignClient;
import app.nike.models.Email;
import app.nike.models.Hr;
import app.nike.models.User;
import app.nike.utility.GenericUtility;

@Service
public class HrServiceImpl implements HrService {

	@Autowired
	private GestioneUtenteFeignClient gestioneUtenteFeign;

	@Autowired
	private HrCrudFeignClient hrCrudFeign;
	
	@Autowired
	private GestioneEmailFeignClient gestioneEmail;

	@Override
	public Hr addHr(Hr hr) {
		Hr hrInserito=null;
		Hr hrGiaInserito = hrCrudFeign.getByUsername(hr.getUsername());
		User utenteGiaInserito = gestioneUtenteFeign.trovaUserDaUsernameOEmail(hr.getUsername());

		try {
			if (hrGiaInserito == null && utenteGiaInserito == null) {
				 hrInserito = hrCrudFeign.salvaHr(hr);
				if (hrInserito == null) {
					throw new Exception("Errore nel Salvataggio dell'HR");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			String password = GenericUtility.randomPassword();
			User userInserito = gestioneUtenteFeign
					.createUser(new User(hr.getUsername(), password, hr.getEmail(), true, hr.getRoles()));
			if (userInserito == null) {
				throw new Exception("Errore nel Salvataggio dell'UTENTE");
			}
			Email email=new Email();
			email.setOggetto("Credenziali Accesso nToWeb");
			email.setCorpo("Ciao "+hr.getUsername()+"\n\n ecco le tue credenziali di accesso al sito nToWeb\n\n"
					+ "Username:"+hr.getUsername()+"\nPassword:"+password+"\n\n Saluti, nToWeb!");
			email.setDestinatario(hr.getEmail());
			try {
				gestioneEmail.inviaEmailProvaDiverso(email);
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return hrInserito;
	}

	@Override
	public Hr modificaHr(Hr hr) {
		Hr hrPresente=null;
		Hr hrModificato=null;
		User userPresente=null;
		User userModificato=null;
		String passwordChiara=null;
		try {
			hrPresente=hrCrudFeign.getByUsername(hr.getUsername());
			if(hrPresente==null) throw new Exception("HR non trovato sul DataBase");
			
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
		try {
			userPresente=gestioneUtenteFeign.trovaUserDaUsernameOEmail(hr.getUsername());
			System.out.println(userPresente.getUsername());
			if(userPresente==null) throw new Exception("Utente non trovato sul DataBase");
			
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
		hrPresente.setCognome(hr.getCognome());
		hrPresente.setNome(hr.getNome());		
		hrPresente.setEmail(hr.getEmail());
		try {
			hrModificato=hrCrudFeign.updateHr(hrPresente);
			if(hrModificato==null) throw new Exception("Errore nel salvataggio dell'Hr modificato sul DataBase");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		if(hr.getEmail().contains("@"))	userPresente.setEmail(hr.getEmail());
		if(hr.getPassword()!=null) { 
			userPresente.setPassword(hr.getPassword());
			passwordChiara=hr.getPassword();
		}
		else {
			passwordChiara="**Password non Modificata**";
		}
		userPresente.setRoles(hr.getRoles());
		try {
			userModificato=gestioneUtenteFeign.updateUser(userPresente);
			if(userModificato==null) throw new Exception("Errore nella Modifica dell'Utente sul DataBase");
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		Email emailDaInviare=GenericUtility.registrationEmail(hrModificato.getUsername(),hrModificato.getNome(), hrModificato.getCognome(), hrModificato.getEmail(), passwordChiara);
		
		Thread thread = new Thread() {
			public void run() {
				gestioneEmail.inviaEmailProvaDiverso(emailDaInviare);
			}
		};
		thread.start();
		
		
		return hrModificato;
	}

	@Override
	public List<Hr> trovaTutti() {
		return hrCrudFeign.leggiHr();
		
	}



	@Override
	public boolean cancellaHr(String username) {
		Hr hrPresente=null;
		Hr hrCancellato=null;
		User userPresente=null;
		User userCancellato=null;
		try {
			hrPresente=hrCrudFeign.getByUsername(username);
			if(hrPresente==null) throw new Exception("HR non trovato sul DataBase");
			
		}catch (Exception e){
			System.out.println(e.getMessage());
			return false;
		}
		try {
			userPresente=gestioneUtenteFeign.trovaUserDaUsernameOEmail(username);
			if(userPresente==null) throw new Exception("Utente non trovato sul DataBase");
			
		}catch (Exception e){
			System.out.println(e.getMessage());
			return false;
		}
		hrCrudFeign.rimuoviHr(hrPresente.getUsername());
		try {
		hrCancellato=hrCrudFeign.getByUsername(username);
		if(hrCancellato!=null) throw new Exception("Errore nella Cancellazione dell'HR dal DataBase");
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		gestioneUtenteFeign.deleteUtente(username);
		try {
			userCancellato=gestioneUtenteFeign.trovaUserDaUsernameOEmail(hrPresente.getUsername());
			if(userCancellato!=null) throw new Exception("Errore nella Cancellazione dell'User dal DataBase");
			}catch(Exception e) {
				System.out.println(e.getMessage());
				return false;
			}	
		
		return true;
		
	}

}
